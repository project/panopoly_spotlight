<?php

declare(strict_types=1);

namespace Drupal\panopoly_spotlight\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\entity_reference_revisions\Plugin\Field\FieldFormatter\EntityReferenceRevisionsFormatterBase;
use Drupal\file\FileInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\media\MediaInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @FieldFormatter(
 *   id="panopoly_spotlight_formatter",
 *   label=@Translation("Spotlight"),
 *   description=@Translation("Displays the spotlight"),
 *   field_types={"entity_reference_revisions"},
 * )
 *
 * @see \Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter
 */
final class Spotlight extends EntityReferenceRevisionsFormatterBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The image style entity storage.
   *
   * @var \Drupal\image\ImageStyleStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->currentUser = $container->get('current_user');
    $instance->imageStyleStorage = $container->get('entity_type.manager')->getStorage('image_style');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'image_style' => 'panopoly_images_full',
      'pause_play_buttons' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $image_styles = image_style_options(FALSE);
    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Image Styles'),
      Url::fromRoute('entity.image_style.collection')
    );
    $element['image_style'] = [
      '#title' => t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable() + [
        '#access' => $this->currentUser->hasPermission('administer image styles'),
      ],
    ];
    $element['pause_play_buttons'] = [
      '#title' => t('Display pause/play button'),
      '#type' => 'radios',
      '#default_value' => $this->getSetting('pause_play_buttons'),
      '#options' => [
        1 => t('Yes'),
        0 => t('No'),
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];
    $image_styles = image_style_options(FALSE);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    $image_style_setting = $this->getSetting('image_style');
    if (isset($image_styles[$image_style_setting])) {
      $summary[] = t('Image style: @style', ['@style' => $image_styles[$image_style_setting]]);
    }
    else {
      $summary[] = t('Original image');
    }

    $pause_play_buttons = (bool) $this->getSetting('pause_play_buttons');
    if ($pause_play_buttons) {
      $summary[] = t('Display pause/play button');
    }
    else {
      $summary[] = t('Hide pause/play button');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    assert($items instanceof EntityReferenceFieldItemListInterface);
    $elements = [];

    $entity = $items->getEntity();

    $duration = 5;
    if ($entity->hasField('field_panopoly_spotlight_seconds')) {
      $duration = (int) $entity->get('field_panopoly_spotlight_seconds')->value;
    }
    $pager = 'full';
    if ($entity->hasField('field_panopoly_spotlight_pager')) {
      $pager = $entity->get('field_panopoly_spotlight_pager')->value;
    }

    $cacheable_metadata = new CacheableMetadata();
    $referenced_entities = $this->getEntitiesToView($items, $langcode);
    $referenced_entities = array_filter($referenced_entities, static function (Paragraph $slide) {
      return !$slide->get('field_panopoly_spotlight_image')->isEmpty();
    });

    $image_style_setting = $this->getSetting('image_style');
    $image_build_base = [
      '#theme' => 'image',
    ];
    if (!empty($image_style_setting)) {
      $image_style = $this->imageStyleStorage->load($image_style_setting);
      $cacheable_metadata->addCacheableDependency($image_style);
      $image_build_base = [
        '#theme' => 'image_style',
        '#style_name' => $image_style_setting,
      ];
    }

    $slides = [];
    foreach ($referenced_entities as $slide) {
      $cacheable_metadata->addCacheableDependency($slide);

      $link = $slide->get('field_panopoly_spotlight_link')->uri;
      $media = $slide->get('field_panopoly_spotlight_image')->entity;
      if (!$media instanceof MediaInterface) {
        continue;
      }
      $source_field_name = $media->getSource()->getConfiguration()['source_field'];
      $image_item = $media->get($source_field_name)->first();
      if (!$image_item instanceof ImageItem) {
        continue;
      }
      $image = $image_item->entity;
      if (!$image instanceof FileInterface) {
        continue;
      }

      $slides[] = [
        'title' => $slide->get('field_panopoly_spotlight_title')->value,
        'description' => $slide->get('field_panopoly_spotlight_body')->value,
        'url' => $link,
        'image' => $image_build_base + [
          '#uri' => $image->getFileUri(),
          '#alt' => $image_item->alt,
          '#width' => $image_item->width,
          '#height' => $image_item->height,
        ],
      ];
    }

    $elements[0] = [
      '#theme' => 'panopoly_spotlight',
      '#attached' => [
        'library' => [
          'panopoly_spotlight/panopoly_spotlight',
        ],
      ],
      '#items' => $slides,
      '#duration' => $duration * 1000,
      '#pager_style' => $pager,
      '#pause_play_buttons' => (bool) $this->getSetting('pause_play_buttons'),
    ];
    $cacheable_metadata->applyTo($elements[0]);

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    return $field_definition->getName() === 'field_panopoly_spotlight_slides';
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $dependencies = parent::calculateDependencies();
    $style_id = $this->getSetting('image_style');
    /** @var \Drupal\image\ImageStyleInterface $style */
    if ($style_id && $style = ImageStyle::load($style_id)) {
      // If this formatter uses a valid image style to display the image, add
      // the image style configuration entity as dependency of this formatter.
      $dependencies[$style->getConfigDependencyKey()][] = $style->getConfigDependencyName();
    }
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function onDependencyRemoval(array $dependencies): bool {
    $changed = parent::onDependencyRemoval($dependencies);
    $style_id = $this->getSetting('image_style');
    /** @var \Drupal\image\ImageStyleInterface $style */
    if ($style_id && $style = ImageStyle::load($style_id)) {
      if (!empty($dependencies[$style->getConfigDependencyKey()][$style->getConfigDependencyName()])) {
        $replacement_id = $this->imageStyleStorage->getReplacementId($style_id);
        // If a valid replacement has been provided in the storage, replace the
        // image style with the replacement and signal that the formatter plugin
        // settings were updated.
        if ($replacement_id && ImageStyle::load($replacement_id)) {
          $this->setSetting('image_style', $replacement_id);
          $changed = TRUE;
        }
      }
    }
    return $changed;
  }

}
